package edu.sjsu.android.mylocations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class LocationsDB extends SQLiteOpenHelper {
    public static final String DATABASE_TABLE = "Locations";
    public static final String DATABASE_NAME = "My Locations";
    public static final int DATABASE_VERSION = 1;

    // Define SQLite database columns
    public static final String ID_COLUMN = "_id";
    public static final String LONG_COLUMN = "longitude";
    public static final String LAT_COLUMN = "latitude";
    public static final String ZOOM_LEVEL_COLUMN = "zoom";

    public SQLiteDatabase db;
    public static final String TAG = "CharacterTable";

    // Create database with 4 fields and primary key on _id
    /**private static final String DATABASE_CREATE = String.format(
            "CREATE TABLE %s (" +
                    "  %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "  %s TEXT," +
                    "  %s TEXT," +
                    "  %s INTEGER);",
            DATABASE_TABLE, ID_COLUMN, LONG_COLUMN, LAT_COLUMN, ZOOM_LEVEL_COLUMN);**/

    static final String DATABASE_CREATE =
            " CREATE TABLE " + DATABASE_TABLE +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " latitude TEXT NOT NULL, " +
                    " longitude TEXT NOT NULL" +
                    " zoom INTEGER NOT NULL);";


    public LocationsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i(TAG, "Creating DataBase: " + DATABASE_CREATE);
        Log.w("Hellow", "I've create the db");
        sqLiteDatabase.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE); onCreate(sqLiteDatabase);
    }

    public long insertTable(ContentValues values){
        //String lat = location.latitude + "";
        //String lng = location.longitude + "";
        //ContentValues newValues = new ContentValues();

        /**newValues.put(LocationsDB.LAT_COLUMN, lat);
        newValues.put(LocationsDB.ZOOM_LEVEL_COLUMN, 8);
        newValues.put(LocationsDB.LONG_COLUMN, lng);**/
        Log.w("myApp", "Is there an error raj??");
        long rowID = db.insert(LocationsDB.DATABASE_TABLE, null, values);
        return rowID;
    }

    public int deleteAll() {
        return db.delete(DATABASE_TABLE, null, null);
    }

    public Cursor queryAll(){
        String where = null;
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;

        String[] resultColumns = {LocationsDB.ID_COLUMN, LocationsDB.LONG_COLUMN, LocationsDB.LAT_COLUMN, LocationsDB.ZOOM_LEVEL_COLUMN};
        Cursor cursor = db.query(LocationsDB.DATABASE_TABLE, resultColumns, where, whereArgs, groupBy, having, order);

        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String lng = cursor.getString(1);
            String lat= cursor.getString(2);
            int zoom = cursor.getInt(3);
            Log.d("Locations", String.format("%s,%s,%s,%d", id, lng, lat, zoom));
        }
         return cursor;
    }
}
