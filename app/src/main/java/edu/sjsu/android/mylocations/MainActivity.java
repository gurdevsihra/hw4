package edu.sjsu.android.mylocations;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * I used these 2 Stack Overflow links to help:
 * 1. https://stackoverflow.com/questions/43332363/google-maps-not-showing-android
 * 2. https://stackoverflow.com/questions/50461881/java-lang-noclassdeffounderrorfailed-resolution-of-lorg-apache-http-protocolve
 */

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor>{

    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //map.addMarker(new MarkerOptions().position(LOCATION_CS).title("Find Me Here!!"));

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                // Add new marker
                map.addMarker(new MarkerOptions().position(latLng));
                LocationInsertTask task = new LocationInsertTask();

                // Add values for longitude and latitude for insertion into database
                ContentValues values = new ContentValues();

                values.put(LocationsDB.LAT_COLUMN, ("" + latLng.latitude));
                values.put(LocationsDB.LONG_COLUMN, ("" + latLng.longitude));
                values.put(LocationsDB.ZOOM_LEVEL_COLUMN, 8);

                task.doInBackground(values);

                Toast.makeText(
                        MainActivity.this,
                         "Marker is added to the Map",
                        Toast.LENGTH_LONG).show();
                }
        });

        // I used this resource: https://stackoverflow.com/questions/15663793/clear-markers-from-google-map-in-android
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                // From API, clear(): Removes all markers, polylines, polygons, overlays, etc from the map.
                map.clear();

                LocationDeleteTask task = new LocationDeleteTask();

                // No values needed because all markers will be deleted
                task.doInBackground(null);

                Toast.makeText(
                        MainActivity.this,
                        "All markers are removed",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onClick_CS(View v){
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18);
        map.animateCamera(update);
    }

    public void onClick_Univ(View v){
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14);
        map.animateCamera(update);
    }

    public void onClick_City(View v){
        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 10);
        map.animateCamera(update);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this, LocationsContentProvider.CONTENT_URI,null,null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {

        int locationCount = 0;
        double lat = 0;
        double lng = 0;
        int zoom = 0;
        if(data != null){
            locationCount = data.getCount();
            data.moveToFirst();
        }else{
            locationCount = 0;
        }

        for(int i = 0; i<locationCount; i++){
            lng = data.getDouble( data.getColumnIndex("longitude") );
            lat=  data.getDouble( data.getColumnIndex("latitude") );
            zoom = data.getInt( data.getColumnIndex("zoom") );
            LatLng latLng = new LatLng(lat, lng);
            map.addMarker(new MarkerOptions().position(latLng));
            data.moveToNext();
        }

        // I used this resource: https://stackoverflow.com/questions/41508555/how-to-change-camera-position-in-google-map-using-android
        if(locationCount > 0){

            // Will use the last initialized values of latitude and longitude
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), zoom));
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    private class LocationInsertTask extends AsyncTask<ContentValues, Void, Void>{

        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            if(contentValues.length == 1){
                // use contentResolver.insert
                ContentValues values = contentValues[0];
                Uri uri = getContentResolver().insert(LocationsContentProvider.CONTENT_URI, values);
            }
            return null;
        }
    }

    private class LocationDeleteTask extends AsyncTask<ContentValues, Void, Void>{

        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            getContentResolver().delete(LocationsContentProvider.CONTENT_URI, "",null);
            return null;
        }
    }

}
