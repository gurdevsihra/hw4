package edu.sjsu.android.mylocations;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * The following resource greatly helped me understand Content Providers: https://www.tutorialspoint.com/android/android_content_providers.htm
 *
 * https://stackoverflow.com/questions/39825125/android-recyclerview-cursorloader-contentprovider-load-more
 *
 */

public class LocationsContentProvider extends ContentProvider {

    // The name for the content provider is also used as the authority in the manifest file
    static final String PROVIDER_NAME = "edu.sjsu.android.mylocations.LocationsContentProvider";
    static final String URL = "content://" + PROVIDER_NAME + "/locations";
    static final Uri CONTENT_URI= Uri.parse(URL);

   /** static final String _ID = "_id";
    public static final String LONG = "Longitude";
    public static final String LAT = "Latitude";
    public static final String ZOOM = "Zoom Level";**/

    private LocationsDB db;
    private SQLiteDatabase database;

    static final int LOCATIONS = 1;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "locations", LOCATIONS);
    }

    @Override
    public boolean onCreate() {
        Log.w("myApp", "I'm literallly here");
        Context context = getContext();
        db = new LocationsDB(context);
        database = db.getWritableDatabase();
        return (database == null)? false:true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Cursor c = db.queryAll();
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){ /**
         * Get all student records
         */
            case LOCATIONS:
                return "vnd.android.cursor.dir/vnd.example.locations"; /**
             * Get a particular student
             */
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri); }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {

            long rowID = db.insertTable(contentValues);
            /**

            // Extract longitude and latitude values from contentValues
            double latitude = contentValues.getAsDouble("latitude");
            double longitude = contentValues.getAsDouble("longitude");
            LatLng location = new LatLng(latitude, longitude);
**/
            Log.w("myApp", "I'm literallly here trying to query");

            if (rowID > 0) {
                Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
                //getContext().getContentResolver().insert(Uri.parse(URL), new ContentValues());
                return _uri;
            }

        throw new SQLException("Failed to add a record into " + uri);
    }


    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int count = db.deleteAll();
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
